var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');

var env = process.env.NODE_ENV;

var config = {};

config.resolve = {
  extensions: ['', '.js', '.jsx']
};

if (env == 'development') {
  config.devServer = {
    contentBase: __dirname,
    historyApiFallback: true,
    hot: true
  };
}

if (env == 'development') {
  config.devtool = 'eval-source-map';
}

if (env == 'development') {
  config.entry = [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './src/index'
  ];
} else if (env == 'production') {
  config.entry = [
    './src/index'
  ];
}

if (env == 'development') {
  config.output = {
    path: __dirname,
    filename: 'index.js',
    publicPath: '/'
  };
} else if (env == 'production') {
  config.output = {
    path: __dirname,
    filename: 'dist/index.js',
    publicPath: '/'
  };
}

if (env == 'development') {
  config.plugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new DashboardPlugin()
  ];
} else if (env == 'production') {
  config.plugins = [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new ExtractTextPlugin('dist/styles.css')
  ];
}

if (env == 'development') {
  config.module = {
    loaders: [{
      test: /\.jsx?$/,
      loaders: [
        'babel'
      ],
      include: path.join(__dirname, 'src')
    }, {
      test: /\.scss$|\.css$/,
      loaders: [
        'style',
        'css?sourceMap&modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
        'sass?sourceMap'
      ]
    }]
  };
} else if (env == 'production') {
  config.module = {
    loaders: [{
      test: /\.jsx?$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'src')
    }, {
      test: /\.scss$|\.css$/,
      loader: ExtractTextPlugin.extract(
        'css?modules&importLoaders=1&localIdentName=[hash:base64:5]!sass'
      )
    }]
  };
}

module.exports = config;