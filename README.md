# sample-editor
something like Maschine.

![http://i.imgur.com/uaGukJT.gif](http://i.imgur.com/uaGukJT.gif)

---

## instructions

### install dependencies
```
npm install
```
this will install all application requisites to develop and run the application.

### run a development server
```
npm run-script serve:development
```
this will start a hot reloading development server at [http://localhost:8080](http://localhost:8080).

### build & package
```
npm run-script build:production
```
this will package all assets and create a static application in `dist/`.

### serve packaged build
```
npm run-script serve:production
```
this will start a static server hosting the build application in `dist/` at [http://localhost:8080](http://localhost:8080).