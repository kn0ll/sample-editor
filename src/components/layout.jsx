import React, { PropTypes, PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import { addAudioSource, setAudioSourceName } from 'redux-audio-sources/lib/actions';
import { addPalette, setPaletteName,
  setPaletteSourceId } from '../actions/palettes';

import AudioSourcesList from 'react-audio-sources-list';
import Palettes from './palettes';

import audioSourcesListStyles from 'react-audio-sources-list/lib/styles.css';
import layoutStyles from './layout.scss';

const mapStateToProps = (state) => {
  return {
    audioSources: state.get('audioSources'),
    palettes: state.get('palettes')
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addAudioSource: (name, size, buffer) => {
      dispatch(addAudioSource({ name, size, buffer }));
    },
    setAudioSourceName: (id, name) => {
      dispatch(setAudioSourceName({ id, name }));
    },
    addPalette: (name) => {
      dispatch(addPalette({ name }));
    },
    setPaletteName: (id, name) => {
      dispatch(setPaletteName({ id, name }));
    },
    setPaletteSourceId: (id, sourceId) => {
      dispatch(setPaletteSourceId({ id, sourceId }));
    }
  };
};

@connect(mapStateToProps, mapDispatchToProps)
@DragDropContext(HTML5Backend)
class Layout extends PureComponent {

  static propTypes = {
    audioSources: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        size: PropTypes.number.isRequired,
        buffer: PropTypes.object.isRequired
      })
    ).isRequired,
    palettes: ImmutablePropTypes.listOf(
      ImmutablePropTypes.mapContains({
        id: PropTypes.string.isRequired,
        name: PropTypes.string,
        sourceId: PropTypes.string
      })
    ).isRequired,
    addAudioSource: PropTypes.func.isRequired,
    setAudioSourceName: PropTypes.func.isRequired,
    addPalette: PropTypes.func.isRequired,
    setPaletteName: PropTypes.func.isRequired,
    setPaletteSourceId: PropTypes.func.isRequired,
    decodeAudioData: PropTypes.func.isRequired
  };

  render() {
    const { audioSources, addAudioSource, setAudioSourceName,
      palettes, addPalette, setPaletteName, setPaletteSourceId,
      decodeAudioData } = this.props;

    return (
      <div className={layoutStyles['layout']}>
        <AudioSourcesList
          styles={audioSourcesListStyles}
          audioSources={audioSources}
          addAudioSource={addAudioSource}
          setAudioSourceName={setAudioSourceName}
          decodeAudioData={decodeAudioData}
          handleSourceItemDropped={setPaletteSourceId} />
        <Palettes
          audioSources={audioSources}
          palettes={palettes}
          addPalette={addPalette}
          setPaletteName={setPaletteName} />
      </div>
    );
  }

}

export default Layout;
