import React, { PureComponent, PropTypes } from 'react';
import Immutable from 'immutable';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import { DropTarget } from 'react-dnd';

import styles from './index.scss';

class Waveform extends PureComponent {

  static propTypes = {
    buffer: PropTypes.object.isRequired,
    height: PropTypes.number.isRequired,
    zoom: PropTypes.number.isRequired,
    position: PropTypes.number.isRequired,
    ratio: PropTypes.number.isRequired
  };

  drawBufferChannel(ctx, ratio, width, height, buffer, channel, color) {
    const { zoom, position } = this.props;
    const mid = Math.floor((height * ratio) / 2);

    const samples = buffer.getChannelData(channel);
    const samplesLength = samples.length;

    // if zoom is 0, we draw every sample
    // if zoom is 1, the whole buffer fits in the width
    const numSamplesPerPixel = Math.max(Math.floor(samplesLength / (width * ratio) * zoom), 1);
    const numWindowedSamples = (width * ratio) * numSamplesPerPixel;

    // if position is 0, we grab numWindowedSamples off the left of the array
    // if position is 1, we grab numWinodwedSamples off the right of the array
    const posOffset = Math.floor((samplesLength - numWindowedSamples) / (1 / position));
    const windowedSamples = samples.slice(posOffset, posOffset + numWindowedSamples);
    
    const normalizedSamples = [];

    let group = [];
    for (let i = 0; i < windowedSamples.length; i++) {
      group.push(windowedSamples[i]);
      if (!(i % (numSamplesPerPixel * ratio))) {
        normalizedSamples.push(group.reduce((a, b) => Math.abs(a) + Math.abs(b)) / group.length);
        group = [];
      }
    }

    for (let i = 0; i < normalizedSamples.length; i++) {
      const sample = normalizedSamples[i];
      const scaled = Math.floor(sample * mid);
      ctx.beginPath();
      ctx.moveTo(i, (mid - scaled) / ratio);
      ctx.lineTo(i, (mid + scaled) / ratio);
      ctx.strokeStyle = color;
      ctx.stroke();
    }
  }

  draw() {
    const canvas = ReactDOM.findDOMNode(this);
    const ctx = canvas.getContext('2d');
    const { buffer, height, ratio } = this.props;
    const width = parseInt(getComputedStyle(canvas).width);
    const colors = ['rgba(250, 101, 55, 1)', 'rgba(189, 72, 36, 0.85)'];

    ctx.save();
    ctx.scale(ratio, ratio);
    ctx.clearRect(0, 0, width * ratio, height * ratio);

    for (let i = 0; i < buffer.numberOfChannels; i++) {
      this.drawBufferChannel(ctx, ratio, width, height, buffer, i, colors[i]);
    }

    ctx.restore();
  }

  componentDidMount() {
    const ratio = this.props.ratio;
    const canvas = ReactDOM.findDOMNode(this);
    const width = parseInt(getComputedStyle(canvas).width);

    canvas.setAttribute('width', width * ratio);
    this.draw();
  }

  componentDidUpdate() {
    this.draw();
  }

  render() {
    const { height, ratio } = this.props;

    return (
      <canvas
        height={height * ratio}
        style={{ height: height * ratio, width: '100%' }} />
    );
  }

}

class WaveformController extends PureComponent {

  static propTypes = {
    buffer: PropTypes.object.isRequired,
    height: PropTypes.number.isRequired
  };

  state = {
    zoom: -0.3,
    position: 0
  };

  handlePositionChange(e) {
    const position = parseFloat(e.target.value);
    const setState = this.setState.bind(this);

    e.preventDefault();
    setState({ position: position });
  }

  handleZoomChange(e) {
    const zoom = parseFloat(e.target.value);
    const setState = this.setState.bind(this);

    e.preventDefault();
    setState({ zoom: zoom });
  }

  render() {
    const { buffer, height } = this.props;
    const { zoom, position } = this.state;
    const handlePositionChange = this.handlePositionChange.bind(this);
    const handleZoomChange = this.handleZoomChange.bind(this);

    return (
      <div
        className={styles['waveform-controller']}>
        <Waveform
          buffer={buffer}
          height={height}
          position={position}
          zoom={Math.abs(zoom)}
          ratio={window.devicePixelRatio} />
        <input
          className={styles['position-slider']}
          type="range"
          min="0"
          max="1"
          step="0.01"
          value={position}
          onChange={handlePositionChange} />
        <input
          className={styles['zoom-slider']}
          type="range"
          min="-1"
          max="0"
          step="0.01"
          value={zoom}
          onChange={handleZoomChange} />
      </div>
    )
  }

}

@DropTarget('source', {
  drop(props) {
    return { id: props.id, name: props.name };
  }
}, (connect, monitor) => ({
  dragConnectDropTarget: connect.dropTarget(),
  dragIsOver: monitor.isOver(),
  dragCanDrop: monitor.canDrop()
}))
class PalettesPane extends PureComponent {

  static propTypes = {
    dragConnectDropTarget: PropTypes.func.isRequired,
    dragIsOver: PropTypes.bool.isRequired,
    dragCanDrop: PropTypes.bool.isRequired,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    selected: PropTypes.bool.isRequired,
    source: PropTypes.instanceOf(Immutable.Map)
  };

  render() {
    const { dragCanDrop, dragIsOver, dragConnectDropTarget,
      selected, source } = this.props;
    const dragIsActive = dragCanDrop && dragIsOver;

    return (
      dragConnectDropTarget(
        <li
          className={cx({
            [styles['selected']]: selected,
            [styles['dragover']]: dragIsActive 
          })}>
          {source?
            <WaveformController
              buffer={source.get('buffer')}
              height={100} />:
            'does not have source'}
        </li>
      ) 
    );
  }

}

class PalettesTab extends PureComponent {

  static propTypes = {
    setPaletteName: PropTypes.func.isRequired,
    select: PropTypes.func.isRequired,
    selected: PropTypes.bool.isRequired,
    name: PropTypes.string
  };

  state = {
    isEditing: false
  };

  handleClick(e) {
    const { select } = this.props;
    
    e.preventDefault();
    select();
  }

  handleDblClick(e) {
    const setState = this.setState.bind(this);

    e.preventDefault();
    setState({ isEditing: true });
  }

  handleKeyPress(e) {
    const { setPaletteName } = this.props;
    const setState = this.setState.bind(this);
    const { charCode } = e;
    const name = e.target.value;

    if (charCode == 13) {
      setPaletteName(name || undefined);
      setState({ isEditing: false });
    }
  }

  handleStopEditing(e) {
    this.setState({ isEditing: false });
  }

  componentDidUpdate() {
    const { nameInput } = this.refs;

    if (nameInput) {
      ReactDOM.findDOMNode(nameInput).focus();
    }
  }

  render() {
    const { name, selected } = this.props;
    const { isEditing } = this.state;

    const handleClick = this.handleClick.bind(this);
    const handleDblClick = this.handleDblClick.bind(this);
    const handleKeyPress = this.handleKeyPress.bind(this);
    const handleStopEditing = this.handleStopEditing.bind(this);

    return (
      <li
        className={cx({
          [styles['selected']]: selected
        })}
        onClick={handleClick}
        onDoubleClick={handleDblClick}>
        {isEditing?
          <input
            ref="nameInput"
            type="text"
            defaultValue={name}
            onKeyPress={handleKeyPress}
            onBlur={handleStopEditing} />:
          name || 'Untitled Palette'}
      </li>
    );
  }

}

class Palettes extends PureComponent {

  static propTypes = {
    audioSources: PropTypes.instanceOf(Immutable.List),
    palettes: PropTypes.instanceOf(Immutable.List),
    addPalette: PropTypes.func.isRequired,
    setPaletteName: PropTypes.func.isRequired
  };

  state = {
    selectedPaneId: null
  };

  render() {
    const setState = this.setState.bind(this);
    const { palettes, audioSources, addPalette, setPaletteName } = this.props;
    const { selectedPaneId } = this.state;

    return (
      <div
        className={styles['palettes']}>
        <ul
          className={styles['tabs']}>
          {palettes.map((palette, i) => {
            const id = palette.get('id');
            const name = palette.get('name');

            return <PalettesTab
              key={id}
              setPaletteName={(name) => { setPaletteName(id, name); }}
              select={() => { setState({ selectedPaneId: id }); }}
              selected={id == selectedPaneId || (!selectedPaneId && i == 0)}
              name={name} />;
          })}
          <li
            onClick={() => { addPalette(); }}>
            +
          </li>
        </ul>
        <ul
          className={styles['panes']}>
          {palettes.map((palette, i) => {
            const id = palette.get('id');
            const name = palette.get('name');
            const sourceId = palette.get('sourceId');
            const source = audioSources.find((source) => source.get('id') == sourceId);

            return <PalettesPane
              key={id}
              id={id}
              name={name}
              selected={id == selectedPaneId || (!selectedPaneId && i == 0)}
              source={source} />;
          })}
        </ul>
      </div>
    );
  }

}

export default Palettes;