import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux-immutable';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

import { initialState } from './state';

import audioSourcesReducer from 'redux-audio-sources/lib/reducer';
import palettesReducer from './reducers/palettes';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducer = combineReducers({
  audioSources: audioSourcesReducer,
  palettes: palettesReducer
});

const middleware = [thunk];

if (process.env.NODE_ENV != 'production') {
  middleware.push(createLogger({ collapsed: true, timestamp: false }));
}

const store = createStore(reducer, initialState, composeEnhancers(
  applyMiddleware(...middleware)
));

export default store;