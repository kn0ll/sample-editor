import { createAction } from 'redux-actions';
import uuid from 'node-uuid';

export const addPalette = createAction('ADD_PALETTE', null, () => ({
  id: uuid.v4()
}));
export const setPaletteName = createAction('SET_PALETTE_NAME');
export const setPaletteSourceId = createAction('SET_PALETTE_SOURCE_ID');