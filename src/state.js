import Immutable from 'immutable';
import { handleActions } from 'redux-actions';

import audioSourcesState from 'redux-audio-sources/lib/state';

const initialState = Immutable.Map({
  audioSources: audioSourcesState,
  palettes: Immutable.List()
});

const createReducer = (stateKey, actions) => {
  return handleActions(actions, initialState.get(stateKey));
};

export { initialState, createReducer };