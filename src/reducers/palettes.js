import { Map } from 'immutable';

import { handleActions } from 'redux-actions';

import { addPalette, setPaletteName,
  setPaletteSourceId } from '../actions/palettes';

import { createReducer } from '../state';

let palettesReducer = createReducer('palettes', {

  [addPalette]: (state, action) => {
    return state.push(Map({
      id: action.meta.id,
      name: action.payload.name,
      sourceId: null
    }));
  },

  [setPaletteName]: (state, action) => {
    return state.setIn([state.findIndex((item) => 
      item.get('id') == action.payload.id
    ), 'name'], action.payload.name);
  },

  [setPaletteSourceId]: (state, action) => {
    return state.setIn([state.findIndex((item) => 
      item.get('id') == action.payload.id
    ), 'sourceId'], action.payload.sourceId);
  }

});

export default palettesReducer;