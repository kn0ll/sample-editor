import { AppContainer } from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import Layout from './components/layout';

import styles from './index.scss';

import store from './store';

const audioContext = new AudioContext();
const rootEl = document.getElementById('root');

ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <Layout decodeAudioData={audioContext.decodeAudioData.bind(audioContext)} />
    </Provider>
  </AppContainer>,
  rootEl
);

if (module.hot) {
  module.hot.accept('./components/layout', () => {
    const NextLayout = require('./components/layout').default;
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <NextLayout decodeAudioData={audioContext.decodeAudioData.bind(audioContext)} />
        </Provider>
      </AppContainer>,
      rootEl
    );
  });
}